@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h1>{{$role->title}}</h1></div>
                <div class="card-body">
                    <ul>
                        <form action="{{ route('roles.update', $role->id)}}"
                        method="post">
                        @csrf
                        @method('patch')
                            <label for="title">role name</label>
                            <input type="text" name="title" id="title" value="{{ $role->id }}">
                            <button type="submit" class="btn btn-outline-light">add!</button>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
