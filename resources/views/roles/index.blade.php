@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h1>Rollen</h1>
                    <a class="btn btn-outline-info btn-sm" href="{{ route('roles.create') }}">make role</a>

                </div>

                <div class="card-body">
                    <ul class="list-group">
                        @foreach($roles as $role)
                            <div class="card">
                                <div class="card-header">
                                    <p>{{$role->toArray()['updated_at']}}</p>
                                    <p>{{$role->updated_at}}<br></p>
                                </div>
                            </div>
                            <li>
                                <a href="{{route("roles.show", $role->id)}}">{{$role->title}}</a>
                                <a class="btn btn-outline-info btn-sm float-end" href="{{route("roles.edit", $role->id)}}">update</a>
                                <form style="display:inline-block;float: right" action="{{ route('roles.destroy', $role->id) }}"
                                method="post">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-outline-danger btn-sm" type="submit">DESTROY</button>

                                </form>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
